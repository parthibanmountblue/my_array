function filter(array, cb) {
    let arr = [];
    for (let index = 0; index < array.length; index++) {
        if(cb(array[index],index,array)==true){
            arr.push(array[index]);
        }
    }return arr;
}
module.exports = filter;