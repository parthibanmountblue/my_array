function reduce(array, callBack, startingValue) {
    if (startingValue !== undefined) {
        let reduce = startingValue;
        for (let index = 0; index < array.length; index++) {
            reduce = callBack(reduce, array[index], index, array);
        }
        return reduce;
    } else {
        let reduce = array[0];
        for (let index = 1; index < array.length; index++) {
            reduce = callBack(reduce, array[index], index, array);
        }
        return reduce
    }
}
module.exports = reduce;