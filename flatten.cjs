function flatten(elements, depth = 1) {
    let flattened = [];
    for (let index = 0; index < elements.length; index++) {
        if (elements[index] == undefined) {
            continue;
        }
        if (Array.isArray(elements[index]) && depth >= 1) {
            flattened = flattened.concat(flatten(elements[index], depth - 1));
        } else {
            flattened.push(elements[index]);
        }
    }
    return flattened;
}

module.exports = flatten;


