function map(array, callback) {
    let myArr = [];
    if (array === undefined) {
        return undefined;
    } else {
        for (let index = 0; index < array.length; index++) {
            myArr.push(callback(array[index], index, array));
        }
    }
    return myArr;
}
module.exports = map;